#! /usr/bin/env python3
# -*- coding: utf-8 -*-
# vim:fenc=utf-8
#
# Copyright © 2015 Adrian Perez <aperez@igalia.com>
#
# Distributed under terms of the MIT license.

def on_document_loaded(webpage):
    # As en example, use the DOM to print the document title
    #print("document-loaded: uri =", webpage.get_uri())
    #document = webpage.get_dom_document()
    #print("document-loaded: title =", document.get_title())
    pass


def is_blocked(uri):
    if ".googlesyndication.com" in uri:
        return True
    if "doubleclick.net" in uri \
       or uri.startswith("https://www.google.com/pagead/") \
       or uri.startswith("https://www.google.fr/pagead/"):
        return True
    if "youtube.com/pagead" in uri \
       or "youtube.com/api/stats/" in uri \
       or "youtube.com/generate_204" in uri \
       or "youtube.com/youtubei/v1/log_event" in uri \
       or "ytimg.com/generate_204" in uri \
       or "googlevideo.com/generate_204" in uri \
       or "youtube.com/ptracking" in uri \
       or "youtube.com/csi_204" in uri \
       or "youtube.com/get_midroll_info" in uri \
       or "youtube.com/get_video_info" in uri and "AdSense-Viral" in uri \
       or "youtube.com/api/stats/ads" in uri:
        return True
    #print(f"NOT BLOCKED: ${uri}")
    return False


def on_send_request(webpage, request, redirect):
    uri = request.get_uri()
    #print("REQUEST : "+uri)
    if is_blocked(uri):
        #print(f"BLOCKING request : {uri}")
        return True


def on_page_created(extension, webpage):
    #print("page-created: extension =", extension)
    #print("page-created: page =", webpage)
    #print("page-created: uri =", webpage.get_uri())
    webpage.connect("document-loaded", on_document_loaded)
    webpage.connect("send-request", on_send_request)


def initialize(extension, arguments):
    print("initialize: extension =", extension)
    #print("initialize: arguments =", arguments)
    extension.connect("page-created", on_page_created)
