#!/usr/bin/python3

import gi
gi.require_version('Gtk', '3.0')
gi.require_version('WebKit2', '4.0')
from functools import lru_cache
from gi.repository import GLib, Gio, GObject, Gtk, WebKit2
import json
import os
import sys
import subprocess
import threading
import extension
from urllib.parse import urlparse
from urllib.parse import parse_qs


def has_different_host(from_url, to_url):
    parsed_from_url = urlparse(from_url)
    parsed_to_url = urlparse(to_url)
    return not parsed_from_url.netloc == parsed_to_url.netloc


def copy_url(url):
    os.system(f'echo "{url}" | xclip -selection clipboard -i')
    os.system('notify-send -u normal -t 5 "URL copied to clipboard"')


default_actions = [{'name': 'Copy URL',
                    'run': copy_url},
                   {'name': 'Chromium',
                    'run': lambda url: os.system(f'chromium "{url}" &> /dev/null & disown')},
                   {'name': 'Firefox',
                    'run': lambda url: os.system(f'firejail firefox "{url}" &> /dev/null & disown')},
                   {'name': 'Youtube-DL',
                    'run': lambda url: os.system(f'kitty --hold youtube-dl "{url}" &> /dev/null & disown')}]

open_windows = 0


context_lock = threading.Lock()
@lru_cache(maxsize=None)
def get_context(context_name):
    mydir = os.path.abspath(os.path.dirname(__file__))
    ctx = WebKit2.WebContext.get_default()
    with context_lock:
        ctx.set_web_extensions_directory(mydir)
        ctx.set_web_extensions_initialization_user_data(GLib.Variant.new_string("test string"))
        cookie_manager = WebKit2.WebContext.get_cookie_manager(ctx)
        cookie_manager.set_accept_policy(WebKit2.CookieAcceptPolicy.ALWAYS)
        cookie_manager.set_persistent_storage(f'/home/arno/.cache/appify_cookies/{context_name}', WebKit2.CookiePersistentStorage.TEXT)
    return ctx

def on_window_destroy(_):
    global open_windows
    open_windows = open_windows - 1
    if open_windows == 0:
        Gtk.main_quit()

def bitwarden_get_credentials(bw_id):
    rc_file = os.getenv('HOME')+"/.appifyrc"
    if not os.path.exists(rc_file):
        print("Missing .appifyrc file in home");
        return
    with open(rc_file, 'r') as f:
        bw_session = f.read().strip()
    bw_command = f'bash -c "source /usr/share/nvm/init-nvm.sh &> /dev/null; OPENSSL_CONF=/dev/null nvm exec --silent node bw get item {bw_id} --session {bw_session}"'
    bw_output = subprocess.check_output(bw_command, shell=True, universal_newlines=True)
    bw_dict = json.loads(bw_output)
    return bw_dict['login']['username'], bw_dict['login']['password']

def bitwarden_fill_login_form(webview, username, password):
    webview.run_javascript("""
    function changeValue(input,value){
        var nativeInputValueSetter = Object.getOwnPropertyDescriptor(
            window.HTMLInputElement.prototype,
            "value"
            ).set;
        nativeInputValueSetter.call(input, value);

        var inputEvent = new Event("input", { bubbles: true });
        input.dispatchEvent(inputEvent);
    }""")
    webview.run_javascript(f"""
        changeValue(document.querySelector('input[autocomplete=username]'), '{username}');
    """)
    webview.run_javascript(f"""
        changeValue(document.querySelector('input[autocomplete=password], input[autocomplete=current-password]'), '{password}');
    """)

bw_credentials = {}
def create_window(name, url,
                  actions=[],
                  bitwarden_id=None,
                  window_class=None,
                  cookie_dir=None,
                  related_view=None,
                  decorated=True, icon_path=None,
                  width=1024, height=768,
                  zoom_level=1,
                  new_window=has_different_host):
    def fill_bw_credentials():
        global bw_credentials
        bw_username, bw_password = bitwarden_get_credentials(bitwarden_id)
        bw_credentials[name] = { 'username': bw_username, 'password': bw_password }
    if bitwarden_id:
        threading.Thread(target=fill_bw_credentials, daemon=True).start()
    print(f"CREATE WINDOW {url}");
    if related_view:
        print("WITH RELATED");
    global open_windows
    global app
    window = Gtk.ApplicationWindow()
    window.set_application(app)
    window.set_default_size(width, height)
    window.set_wmclass(window_class or name, window_class or name)
    window.set_title(name)
    window.set_decorated(decorated)
    window.connect('destroy', on_window_destroy)

    if icon_path:
        window.set_icon_from_file(icon_path)

    def on_load_changed(webview, event):
        if event == WebKit2.LoadEvent.FINISHED:
            resource = webview.get_main_resource()
            webview.run_javascript("""
              if (document.location.href.endsWith("-/boards")) {
                  document.getElementsByClassName("nav-sidebar")[0].remove();
                  setTimeout(function () {
                                document.querySelectorAll("a.dashboard-shortcuts-issues")[0].href="https://gitlab.com/mayane-labs/labs-main/-/issues?assignee_username=arnoo";
                                document.querySelectorAll('li.user-counter')[0].insertAdjacentHTML('beforebegin', '<li class="nav-item"><a href="https://gitlab.com/mayane-labs/labs-main/-/pipelines"><svg class="s16" data-testid="rocket-icon"><use href="/assets/icons-f14c6b7def1f34543f596e9dc871ae257a333de2bc9d5ac8bc9be0a805523a7f.svg#rocket"></use></svg></a></li>');
                                document.getElementsByClassName("page-with-icon-sidebar")[0].style.paddingLeft=0;
                                for (board of document.getElementsByClassName("board")) {
                                    board.style.width = "307px";
                                    //title = board.querySelectorAll(".board-title-text > .gl-label-text")[0].innerHtml;
                                    //board.querySelectorAll(".issue-count-badge")[0].onClick(function (evt) {
                                    //    alert(title);
                                    //    })
                                }}, 2000);
                  document.getElementsByClassName("alert-wrapper")[0].remove(); // breadcrumbs
                  document.body.style.backgroundColor = "rgba(0,0,0,0)";
              } else if (document.location.href.includes("/merge_requests/")) {
                  document.getElementsByClassName("right-sidebar")[0].remove();
              } else if (document.location.href.includes("youtube.com")) {
                  ytInitialPlayerResponse.adPlacements = undefined;
                  ytInitialPlayerResponse = false;
              }
//              if (document.location.href.includes("mail.google.com")) {
//                  document.querySelectorAll("a[target='_blank']").forEach(function (anchor) {
//                      delete(anchor.onclick);
//                  });
//              }
            """)

    def append_menu_items(webview, context_menu, hit_result_event, event):
        for action in (default_actions + actions):
            target_url = event.get_link_uri() or webview.get_uri()
            if 'active' not in action\
               or action['active'](target_url):
                gtk_action = Gtk.Action(action['name'],
                                        action['name'],
                                        action['name'],
                                        Gtk.STOCK_NEW)
                gtk_action.connect('activate', lambda gaction,
                                                      action=action,
                                                      target_url=target_url:
                                                  action['run'](target_url))
                option = WebKit2.ContextMenuItem().new(gtk_action)
                context_menu.append(option)

    def on_popup(webview, action):
        target_url = action.get_request().get_uri()
        print(f'POPUP URL: "{target_url}"')
        if target_url == "" \
           or target_url == "about:blank" \
           or target_url == "_blank":
            popup = create_window(name, target_url, related_view=webview, cookie_dir=cookie_dir, actions=actions, icon_path=icon_path, zoom_level=zoom_level)
            popup.load_uri(target_url)
            popup.show()
            return popup
        else:
            target_url = clean_url(target_url)
            if new_window(webview.get_uri(), target_url):
                dispatch(target_url)
            else:
                dispatch(target_url, related_view=webview)

    def on_decide_policy(webview, decision, decision_type=None):
        if decision_type != WebKit2.PolicyDecisionType.NAVIGATION_ACTION:
            return False # let the decision be handled by other handlers
        #print("DECISION")
        from pprint import pprint
        #pprint(dir(decision))
        #pprint(dir(decision_type))
        # Always accept response
        #if decision_type == WebKit2.PolicyDecisionType.RESPONSE:
        #    mime_type = decision.get_response().props.mime_type
        #    if "application/" in mime_type:
        #        decision.download()
        #    else:
        #        decision.use()
        #    return False

        target_url = decision.get_request().get_uri()
        #print(target_url)

        if target_url == 'about:blank' or target_url == '_blank':
            decision.ignore() # not sure why we need this on top of return True…
            return True  # stop other handlers from being invoked for the event
        if decision.get_navigation_action().is_user_gesture() \
           and (decision.get_mouse_button() == 2
                or new_window(webview.get_uri(), target_url)):
            dispatch(clean_url(target_url))
            decision.ignore() # not sure why we need this on top of return True…
            return True # stop other handlers from being invoked for the event
        else:
            return False # let the decision be handled by other handlers

    def on_resource_load(web_view, resource, request):
        #print("RESOURCE LOAD")
        #print(resource.get_uri())
        return True
        request.set_uri("about:blank")

    def on_title(webview, frame):
        window.set_title(webview.get_title())

    def on_favicon(webview, frame):
        print("ON FAVICON")
        print(webview.get_favicon())

    def toggle_action(window, prop, on_callback, off_callback):
        if getattr(window, prop, False):
            off_callback()
            setattr(window, prop, False);
        else:
            on_callback()
            setattr(window, prop, True);

    def on_drag_data_received(webview, context, x, y, data, info, time):
        print("DDR")
        for i in data.get_uris():
            print(i)
        print(context)
        print(data)
        print(info)

    def on_shortcut(action, param):
        string = param.get_string()
        if string == "bitwarden":
            global bw_credentials
            bitwarden_fill_login_form(webview,
                                      bw_credentials[name]['username'],
                                      bw_credentials[name]['password'])
        elif string == "devtools":
            toggle_action(window,
                          '__has_inspector',
                          webview.get_inspector().show,
                          webview.get_inspector().close)
        elif string == "fullscreen":
            toggle_action(window,
                          '__is_fullscreen',
                          window.fullscreen,
                          window.unfullscreen)
        elif string == "print":
            op = WebKit2.PrintOperation.new(webview)
            op.run_dialog()
        elif string == "reload":
            webview.reload()
        elif string == "stop":
            webview.stop_loading()
        elif string == "zoom_incr":
            webview.set_zoom_level(webview.get_zoom_level()+0.1)
        elif string == "zoom_decr":
            webview.set_zoom_level(webview.get_zoom_level()-0.1)


    def on_permission_request(webview, request):
        #print("PERMSSION REQUEST !!!!")
        request.allow()

    scrolled_window = Gtk.ScrolledWindow()
    if related_view:
        webview = WebKit2.WebView.new_with_related_view(related_view)
    else:
        webview = WebKit2.WebView.new_with_context(get_context(cookie_dir or name))


    shortcut_action = Gio.SimpleAction.new('shortcut',
                                           GLib.VariantType.new('s'))
    shortcut_action.connect('activate', on_shortcut)
    app.add_action(shortcut_action)
    app.set_accels_for_action("app.shortcut::bitwarden", ["<Control><Shift>L"])
    app.set_accels_for_action("app.shortcut::devtools", ["F12"])
    app.set_accels_for_action("app.shortcut::fullscreen", ["F11"])
    app.set_accels_for_action("app.shortcut::print", ["<Control>P"])
    app.set_accels_for_action("app.shortcut::reload", ["<Control>R", "F5"])
    app.set_accels_for_action("app.shortcut::stop", ["<Esc>"])
    app.set_accels_for_action("app.shortcut::zoom_incr", ["<Control>plus"])
    app.set_accels_for_action("app.shortcut::zoom_decr", ["<Control>minus"])

    webview.connect('permission-request', on_permission_request)
    webview.connect('context-menu', append_menu_items)
    webview.connect('decide-policy', on_decide_policy)
    webview.connect("drag-data-received", on_drag_data_received)
    webview.connect('create', on_popup)
    webview.connect('load_changed', on_load_changed)
    webview.connect('notify::title', on_title)
    webview.connect('notify::favicon', on_favicon)
    webview.get_settings().set_enable_write_console_messages_to_stdout(True)
    webview.get_settings().set_property("enable-developer-extras", True)
    webview.set_zoom_level(zoom_level)
    webview.load_uri(url)
    scrolled_window.add(webview)

    window.add(scrolled_window)
    open_windows = open_windows + 1
    window.show_all()
    if related_view:
        return webview


def clean_url(url):
    if url.startswith("https://www.google.com/url"):
        parsed_url = urlparse(url)
        return parse_qs(parsed_url.query)['q'][0]
    return url
    

def dispatch(url, related_view=None):
    if url.startswith("https://gitlab.com") and "/boards" in url:
        return create_window("Gitlab",
                             url,
                             icon_path="/home/arno/.devilspie/gitlab.png",
                             related_view=related_view,
                             decorated=False,
                             window_class="Gitlab-board",
                             width=2475,
                             height=1326,
                             new_window=lambda f, to: True)
    elif url.startswith("https://gitlab.com") \
         or url.startswith("https://www.gitlab.com"):
        return create_window("Gitlab", url,
                             related_view=related_view,
                             decorated=False,
                             width=1223,
                             height=1234,
                             icon_path="/home/arno/.devilspie/gitlab.png")
    elif url.startswith("https://www.fastmail.com") and "/calendar" in url:
        return create_window("Calendar", url,
                             related_view=related_view,
                             decorated=False,
                             cookie_dir="Fastmail",
                             icon_path="/home/arno/.devilspie/agenda.png")
    elif url.startswith("https://www.fastmail.com") and "/mail" in url:
        return create_window("Fastmail", url,
                             related_view=related_view,
                             decorated=False,
                             icon_path="/home/arno/.devilspie/email.png",
                             zoom_level=1.3)
    elif url.startswith("https://www.gmail.com") \
         or url.startswith("https://mail.google.com"):
        return create_window("Gmail-Support" if "ALGkd0x" in url else "Gmail", url,
                             related_view=related_view,
                             bitwarden_id='397bc96f-0610-45d6-8aec-acaf0090d2e5',
                             decorated=False,
                             cookie_dir="Gmail",
                             icon_path="/home/arno/.devilspie/gmail.png" if "ALGkd0x" in url else "/home/arno/.devilspie/gmail_colors.png",
                             new_window=lambda f, to: has_different_host(url, to) and not to.startswith('https://accounts.google.com/'))
#    elif url.startswith("https://www.youtube.com"):
#        return create_window("Youtube", url,
#                             icon_path="/home/arno/.devilspie/youtube.png",
#                             actions=[{'active': lambda url: '/watch' in url,
#                                       'name': 'mpv',
#                                       'run': lambda url: os.system(f'mpv --no-border "{url}"')}])
    elif url.startswith("https://photos.btmx.fr"):
        return create_window("Photoprism", url,
                             related_view=related_view,
                             decorated=False,
                             icon_path="/home/arno/.devilspie/photoprism.svg")
#    elif url.startswith("https://meet.google.com"):
#        return create_window("Meet", url,
#                             related_view=related_view,
#                             decorated=False,
#                             cookie_dir="Gmail",
#                             icon_path="/home/arno/.devilspie/meet.png")
    elif url.startswith("https://web.whatsapp.com"):
        return create_window("Whatsapp", url,
                             related_view=related_view,
                             decorated=False,
                             icon_path="/home/arno/.devilspie/whatsapp.png")
    elif url.startswith("https://maison.btmx.fr"):
        return create_window("Home Assistant", url,
                             related_view=related_view,
                             decorated=False,
                             icon_path="/home/arno/.config/ulauncher/favicons/d7bea8ede09a5dd509de212dc7127b42.png")
    elif url.startswith("https://musique.btmx.fr"):
        return create_window("Navidrome", url,
                             related_view=related_view,
                             decorated=False,
                             icon_path="/home/arno/.devilspie/navidrome.svg")
    elif url == "https://mayanelabs.atlassian.net/jira/software/c/projects/ML/boards/5" \
         or url.startswith("https://id.atlassian.com/") \
         or url.startswith("https://start.atlassian.com/"):
        return create_window("JIRA", url,
                             decorated=False,
                             related_view=related_view,
                             icon_path="/home/arno/.devilspie/jira.svg",
                             window_class="JIRA-board",
                             width=2475,
                             height=1326,
                             new_window=lambda f, to: True)
    elif url.startswith("https://mayanelabs.atlassian.net"):
        return create_window("JIRA", url,
                             related_view=related_view,
                             decorated=False,
                             icon_path="/home/arno/.devilspie/jira.svg")
    elif url.startswith("https://mayanelabs.atlassian.net"):
        return create_window("JIRA", url,
                             related_view=related_view,
                             decorated=False,
                             icon_path="/home/arno/.devilspie/jira.svg")
#    elif url.startswith("https://www.notion.so"):
#        return create_window("Notion", url,
#                             related_view=related_view,
#                             decorated=False,
#                             icon_path="/home/arno/.devilspie/notion.png",
#                             new_window=lambda f, to: has_different_host(url, to) and not to.startswith('https://accounts.google.com/'))
    else:
        os.system(f'firejail firefox "{url}" &> /dev/null & disown')


def dispatch_urls(_):
    for url in sys.argv[1:]:
        print(f"DISPATCH: {url}")
        if url.startswith('appify://'):
            url = url[len('appify://'):]
            # Type about:config into the Location Bar (address bar) and press Enter.
            # Right-click -> New -> Boolean -> Name: network.protocol-handler.expose.foo -> Value -> false (Replace foo with the protocol you're specifying)
            # Next time you click a link of protocol-type foo you will be asked which application to open it with. 
        dispatch(clean_url(url))


app = Gtk.Application()
app.connect("activate", dispatch_urls)
app.run()
